import React from "react";

const ErrorMessage = () => (
  <div>An error occured, please contact help@johnlewis.com.</div>
);

export default ErrorMessage;
