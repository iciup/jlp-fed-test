import { render, screen} from '@testing-library/react'
import ErrorMessage from './error-message'

describe('error message', () => {
    it('renders error message', () => {
        render(<ErrorMessage />)
        
        expect(screen.getByText('An error occured, please contact help@johnlewis.com.')).toBeInTheDocument()
    })
})

  