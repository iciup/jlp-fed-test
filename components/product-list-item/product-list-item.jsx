import styles from "./product-list-item.module.scss";
import Link from "next/link";

const ProductListItem = ({ item }) => {
  return (
    <Link
      key={item.productId}
      href={{
        pathname: "/product-detail/[id]",
        query: { id: item.productId },
      }}
    >
      <span className={styles.link}>
          <div>
            <img className={styles.image} src={item.image} alt="product image" />
          </div>
          <div>{item.title}</div>
          <div className={styles.price}>{item.variantPriceRange?.display.max}</div>
      </span>
    </Link>
  );
};

export default ProductListItem;
