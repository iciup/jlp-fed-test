import { render, screen } from '@testing-library/react'
import ProductListItem from './product-list-item'

const mockItem = {
    productId: 1,
    image: 'asdf',
    title: 'product title',
    variantPriceRange: {
        display: {
            max: '£99.99'
        }
    }

}

describe('product list item', () => {
    it('renders product list item', () => {
        render(<ProductListItem item={mockItem} />)
        const image = screen.getByAltText('product image');

        expect(screen.getByText('product title')).toBeInTheDocument()
        expect(screen.getByText('£99.99')).toBeInTheDocument()
        expect(image).toHaveAttribute('src', 'asdf')
    })
})

  