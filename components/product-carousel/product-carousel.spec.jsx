import { render, screen, waitFor } from '@testing-library/react'
import userEvent from '@testing-library/user-event';
import ProductCarousel from './product-carousel'

describe('product carousel', () => {
    it('renders carousel with one image', () => {
        render(<ProductCarousel images={['asdf']} />)
        const image = screen.getByAltText('product image');

        expect(image).toHaveAttribute('src', 'asdf')
        expect(screen.getByText('1 of 1')).toBeInTheDocument()
    })

    it('renders carousel with two images and navigates to second image', async () => {
        render(<ProductCarousel images={['asdf', 'qwerty']} />)
        expect(screen.getByText('1 of 2')).toBeInTheDocument()
        expect(screen.getAllByAltText('product image').length).toBe(2)

        userEvent.click(screen.getByLabelText('next slide / item'));

        await waitFor(() => {
          expect(screen.getByText('2 of 2')).toBeInTheDocument()
        })
    })
})

  