import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';

const ProductCarousel = ({ images }) => {
  return (
    <Carousel showThumbs={false}>
      {images.map(image => {
        return <div key={image}>
          <img src={image} alt="product image" />
        </div>
      })}
    </Carousel>
  );
};

export default ProductCarousel;
