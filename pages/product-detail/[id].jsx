import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleLeft, faAngleRight } from "@fortawesome/free-solid-svg-icons";

import ProductCarousel from "../../components/product-carousel/product-carousel";
import styles from "../index.module.scss";
import ErrorMessage from '../../components/error-message/error-message';

export async function getServerSideProps(context) {
  try {
    const id = context.params.id;
    const response = await fetch(
      `https://api.johnlewis.com/mobile-apps/api/v1/products/${id}`,
      {
        headers: {
          "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36"
        }
      }
    );
    const data = await response.json();

    return {
      props: { data },
    };
  } catch (error) {
    return {
      props: { 
        data: {
          error: error.message
        } 
      } 
    }
  }
}

const ProductDetail = ({ data }) => {
  if (data.error) {
    return <ErrorMessage />
  }

  return (
    <div>
      <div className={styles['product-details-header']}>
        <a href="javascript:history.back()" className={styles['back-link']}>
          <FontAwesomeIcon icon={faAngleLeft} />
        </a>
        <h1 className={styles['product-title']}>{data.title}</h1>
      </div>
        
      <div className={styles['product-section']}>
        <div className={styles['info-section']}>
          <div className={styles['carousel-section']}>
            <ProductCarousel images={data.media?.images.urls} />
          </div>

          <div className={styles['mobile-pricing-section']}>
            <div>
              <h1 className={styles['display-price']}>£{data.price?.now}</h1>
              <div className={styles['offer']}>{data.displaySpecialOffer}</div>
              <div className={styles['guarantee']}>{data.additionalServices?.includedServices}</div>
            </div>
          </div>

          <div>
            <h3>Product information</h3>

            <p>
              Product code: {data.code}
            </p>
            <div className={styles['product-info']} dangerouslySetInnerHTML={{__html: data.details?.productInformation}}/>
            
            <div className={styles['read-more']}>
              <a href="/">
                <div>Read more</div>

                <div className={styles['arrow']}>
                  <FontAwesomeIcon icon={faAngleRight} />
                </div>
              </a>
            </div>
            
            <h3>Product specification</h3>
            <ul className={styles['product-specification']}>
              {data.details?.features[0].attributes.map((item) => (
                <li key={item.name}>
                  <div className={styles['name']}>
                    {item.name}
                  </div>

                  <div>
                    {item.value}
                  </div>
                </li>
              ))}
            </ul>
          </div>
        </div>

        <div className={styles['desktop-pricing-section']}>
          <div>
            <h1 className={styles['display-price']}>£{data.price?.now}</h1>
            <div className={styles['offer']}>{data.displaySpecialOffer}</div>
            <div className={styles['guarantee']}>{data.additionalServices?.includedServices}</div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProductDetail;
