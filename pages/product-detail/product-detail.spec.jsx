import { render, screen } from "@testing-library/react";

import productData from '../../mockData/data2.json';
import ProductDetail, { getServerSideProps } from './[id]'

describe("product details page", () => {
  beforeEach(() => {
    fetch.resetMocks();
  });

  it("renders an error message", async () => {
    render(<ProductDetail data={{error: 'error'}} />);

    expect(screen.getByText('An error occured, please contact help@johnlewis.com.')).toBeInTheDocument()
  });

  it("renders product details page", async () => {
    fetch.mockResponseOnce(JSON.stringify(productData));
    const response = await getServerSideProps({params: {id: 1}});

    render(<ProductDetail data={response.props.data.detailsData[0]} />);

    expect(screen.getByText('Bosch Serie 2 SMS24AW01G Freestanding Dishwasher, White')).toBeInTheDocument()
    expect(screen.getByText('Product code: 81701106')).toBeInTheDocument()
  });
})
