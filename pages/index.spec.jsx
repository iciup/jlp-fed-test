import { render, screen } from "@testing-library/react";

import productListData from '../mockData/data.json';
import Home, { getServerSideProps } from './index'

describe("product list page", () => {
  beforeEach(() => {
    fetch.resetMocks();
  });

  it("renders an error message", async () => {
    render(<Home data={{error: 'error'}} />);

    expect(screen.getByText('An error occured, please contact help@johnlewis.com.')).toBeInTheDocument()
  });

  it("renders product list page full of products", async () => {
    fetch.mockResponseOnce(JSON.stringify(productListData));
    const response = await getServerSideProps();

    render(<Home data={response.props.data} />);

    expect(screen.getByText(`Dishwashers (${productListData.products.length})`)).toBeInTheDocument()
  });
})
