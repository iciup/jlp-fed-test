import Head from "next/head";
import styles from "./index.module.scss";
import ErrorMessage from '../components/error-message/error-message';
import ProductListItem from '../components/product-list-item/product-list-item';

export async function getServerSideProps() {
  try {
    const response = await fetch(
      "https://api.johnlewis.com/search/api/rest/v2/catalog/products/search/keyword?q=dishwasher&key=AIzaSyDD_6O5gUgC4tRW5f9kxC0_76XRC8W7_mI",
      {
        headers: {
          "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36"
        }
      }
    );
    const data = await response.json();
    return {
      props: {
        data,
      },
    };
  } catch (error) {
    return {
      props: { 
        data: {
          error: error.message
        } 
      } 
    }
  }
}

const Home = ({ data }) => {
  if (data.error) {
    return <ErrorMessage />
  }

  let items = data.products;
  return (
    <div>
      <Head>
        <title>JL &amp; Partners | Home</title>
        <meta name="keywords" content="shopping" />
      </Head>
      <div>
        <h1>Dishwashers {items.length > 0 && `(${items.length})`}</h1>
        <div className={styles.content}>
          {items.map((item) => (<ProductListItem item={item} />))}
        </div>
      </div>
    </div>
  );
};

export default Home;
